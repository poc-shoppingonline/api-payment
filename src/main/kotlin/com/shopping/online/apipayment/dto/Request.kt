package com.shopping.online.apipayment.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

data class Request<T>(
    val content: T
)
