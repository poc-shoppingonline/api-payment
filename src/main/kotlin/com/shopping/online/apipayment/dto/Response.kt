package com.shopping.online.apipayment.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class Response<T>(
    val content: T
)