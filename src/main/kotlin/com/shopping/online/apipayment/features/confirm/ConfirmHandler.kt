package com.shopping.online.apipayment.features.confirm

import com.shopping.online.apipayment.dto.Request
import com.shopping.online.apipayment.features.confirm.models.ConfirmRequest
import com.shopping.online.apipayment.features.confirm.service.ConfirmService
import com.shopping.online.apipayment.utils.ResponseSuccess
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*

@Component
class ConfirmHandler(private val confirmService: ConfirmService) {

    suspend fun confirmPayment(request: ServerRequest) =
//        request.awaitBody<Request<ConfirmRequest>>().let {
//            ResponseSuccess(
//                confirmService.confirmPayment(it.content)
////            ""
//            )
//        }
        request.awaitBody<Request<ConfirmRequest>>().let {
            ServerResponse
                .ok()
                .json()
                .bodyValueAndAwait(confirmService.confirmPayment(it.content))
        }
}