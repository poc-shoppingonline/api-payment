package com.shopping.online.apipayment.features.confirm.models

data class ConfirmRequest(
    val userId: String
)

data class ConfirmResponse(
    val result: String
)