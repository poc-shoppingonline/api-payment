package com.shopping.online.apipayment.features.confirm.service

import com.shopping.online.apipayment.data.entities.*
import com.shopping.online.apipayment.data.repositories.*
import com.shopping.online.apipayment.features.confirm.models.ConfirmRequest
import com.shopping.online.apipayment.features.confirm.models.ConfirmResponse
import org.springframework.stereotype.Service
import org.springframework.transaction.reactive.TransactionalOperator
import org.springframework.transaction.reactive.executeAndAwait


@Service
class ConfirmService(
    private val stockRepository: StockRepository,
    private val userRepository: UserRepository,
    private val cartRepository: CartRepository,
    private val productRepository: ProductRepository,
    private val transactionalOperator: TransactionalOperator,
    private val recordRepository: RecordRepository
) {
    suspend fun confirmPayment(request: ConfirmRequest): ConfirmResponse {
        var res = false
        kotlin.runCatching {
            transactionalOperator.executeAndAwait {
                var sum = 0.00
                getCartData(request.userId)?.let {
                    println("cartdata:$it")
                    for (item in it.list) {
                        println("item:${item.productId},${item.amount}")
                        stockRepository.selectByProductId(item.productId, item.amount)?.let { stock ->
                            getProductData(item.productId)?.let { product ->
                                sum += (product.price * item.amount)
                            }
                            stock.amount = stock.amount - item.amount
                            stockRepository.update(stock)
                        } ?: throw Exception("selectByProductId is null")
                    }
                    println("sum:$sum")
                    val test = userRepository.selectByUserId(request.userId, sum)?.let { user ->
                        user.balance = user.balance - sum
                        println("user update:$user")
                        userRepository.update(user)
                    } ?: throw Exception("Update user error")
                } ?: throw  Exception("getCartData is null")

                res = recordRepository.recordPaymentProduce(
                    RecordMessage(
                        userid = request.userId,
                        totalMoney = sum,
                        timestamp = System.currentTimeMillis(),
                        result = true
                    )
                )
                if (res == false) throw Exception("Kafka Error")
            }
            return ConfirmResponse("SUCCESS")
        }.isFailure.run { return ConfirmResponse("FAILED") }
    }

    private suspend fun getCartData(userid: String): Cart? = cartRepository.getCart(userid)
    private suspend fun getProductData(productid: String): Product? = productRepository.getProduct(productid)

}

