package com.shopping.online.apipayment.features.confirm

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.coRouter

@Configuration
class ConfirmRouter {
    @Bean
    fun confirmRoutes(confirmHandler: ConfirmHandler) = coRouter{
        accept(MediaType.APPLICATION_JSON).nest {
            POST("/confirmpayment", confirmHandler::confirmPayment)
        }
    }
}