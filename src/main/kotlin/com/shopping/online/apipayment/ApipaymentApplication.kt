package com.shopping.online.apipayment

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ApipaymentApplication

fun main(args: Array<String>) {
	runApplication<ApipaymentApplication>(*args)
}
