package com.shopping.online.apipayment.data.repositories

import com.shopping.online.apipayment.utils.RedisKeyBuilder
import com.shopping.online.apipayment.utils.RedisUtils
import org.springframework.stereotype.Repository

@Repository
class ProductRepository(
    private val redisUtils: RedisUtils
) {

    suspend fun getProduct(productId: String) =
        redisUtils.getProduct(RedisKeyBuilder.buildProductKey(productId))
}