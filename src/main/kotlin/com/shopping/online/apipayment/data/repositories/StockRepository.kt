package com.shopping.online.apipayment.data.repositories

import com.shopping.online.apipayment.data.entities.Stock
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.data.r2dbc.core.awaitFirstOrNull
import org.springframework.data.r2dbc.core.awaitRowsUpdated
import org.springframework.data.relational.core.query.Criteria
import org.springframework.stereotype.Repository

@Repository
class StockRepository(
    private val databaseClient: DatabaseClient
) {
    internal suspend fun selectByProductId(productId: String, amount: Int) =
        databaseClient.select()
            .from(Stock::class.java)
            .matching(
                Criteria.where(Stock::productId.name).`is`(productId)
                    .and(Stock::amount.name).greaterThanOrEquals(amount)
            ).fetch()
            .awaitFirstOrNull()

    internal suspend fun update(stock: Stock) =
        databaseClient.update()
            .table(Stock::class.java)
            .using(stock)
            .fetch()
            .awaitRowsUpdated()
}