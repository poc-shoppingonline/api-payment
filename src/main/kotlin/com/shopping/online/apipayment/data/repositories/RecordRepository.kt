package com.shopping.online.apipayment.data.repositories

import com.shopping.online.apipayment.data.entities.RecordMessage
import com.shopping.online.apipayment.utils.KafkaDestinations
import com.shopping.online.apipayment.utils.toMessagePayload
import org.springframework.stereotype.Repository

@Repository
class RecordRepository(
    private val messageStreamRepo: MessageStreamRepo
) {
    suspend fun recordPaymentProduce(payload: RecordMessage) =
        messageStreamRepo.producePayment()
            .send(payload.toMessagePayload())
            .also { println("Produce: ${KafkaDestinations.PRODUCE_PAYMENT} -> payload: ${payload.userid}") }
}