package com.shopping.online.apipayment.data.entities

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("USER_PROFILE")
data class User(
    @Id val userId: String,
    var balance:Double
)