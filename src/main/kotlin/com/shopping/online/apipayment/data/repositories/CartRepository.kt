package com.shopping.online.apipayment.data.repositories

import com.shopping.online.apipayment.utils.RedisKeyBuilder
import com.shopping.online.apipayment.utils.RedisUtils
import org.springframework.stereotype.Repository

@Repository
class CartRepository(
    private val redisUtils: RedisUtils
) {

    suspend fun getCart(userId: String) =
        redisUtils.getCart(RedisKeyBuilder.buildCartKey(userId))
}