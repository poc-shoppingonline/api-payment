package com.shopping.online.apipayment.data.entities

data class Cart(
    val list: MutableList<Mylist>
)

data class Mylist(
    val productId: String,
    val amount: Int
)