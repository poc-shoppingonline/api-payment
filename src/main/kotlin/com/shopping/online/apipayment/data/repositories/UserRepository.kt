package com.shopping.online.apipayment.data.repositories

import com.shopping.online.apipayment.data.entities.Stock
import com.shopping.online.apipayment.data.entities.User
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.data.r2dbc.core.awaitFirstOrNull
import org.springframework.data.r2dbc.core.awaitRowsUpdated
import org.springframework.data.relational.core.query.Criteria
import org.springframework.stereotype.Repository

@Repository
class UserRepository(
    private val databaseClient: DatabaseClient
) {
    internal suspend fun selectByUserId(userId: String, amount: Double) =
        databaseClient.select()
            .from(User::class.java)
            .matching(
                Criteria.where(User::userId.name).`is`(userId)
                    .and(User::balance.name).greaterThanOrEquals(amount)
            ).fetch()
            .awaitFirstOrNull()

    internal suspend fun update(user: User) =
        databaseClient.update()
            .table(User::class.java)
            .using(user)
            .fetch()
            .awaitRowsUpdated()
}