package com.shopping.online.apipayment.data.entities

data class Product(
    val detail: String,
    val price: Double
)