package com.shopping.online.apipayment.data.entities

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("STOCK")
data class Stock (
    @Id val productId: String,
    var amount: Int
)