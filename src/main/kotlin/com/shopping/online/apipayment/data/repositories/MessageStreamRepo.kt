package com.shopping.online.apipayment.data.repositories

import com.shopping.online.apipayment.utils.KafkaDestinations.PRODUCE_PAYMENT
import org.springframework.cloud.stream.annotation.Output
import org.springframework.context.annotation.Bean
import org.springframework.messaging.SubscribableChannel

interface MessageStreamRepo {

    @Output(PRODUCE_PAYMENT)
    fun producePayment(): SubscribableChannel

}