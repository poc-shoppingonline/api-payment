package com.shopping.online.apipayment.data.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.sql.Timestamp

@JsonIgnoreProperties(ignoreUnknown = true)
data class RecordMessage(
    val userid: String,
    val totalMoney: Double,
    val timestamp: Long,
    val result: Boolean
)