package com.shopping.online.apipayment.configs

import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.shopping.online.apipayment.data.entities.Cart
import com.shopping.online.apipayment.data.entities.Product
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveRedisTemplate
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer
import org.springframework.data.redis.serializer.RedisSerializationContext
import org.springframework.data.redis.serializer.StringRedisSerializer

@Configuration
class RedisConfiguration {

    @Bean
    fun redisObjectNodeTemplate(factory: ReactiveRedisConnectionFactory) =
        createReactiveRedisTemplate<ObjectNode>(factory)

    @Bean
    fun redisProductTemplate(factory: ReactiveRedisConnectionFactory) =
        createReactiveRedisTemplate<Product>(factory)

    @Bean
    fun redisCartTemplate(factory: ReactiveRedisConnectionFactory) =
        createReactiveRedisTemplate<Cart>(factory)

    private inline fun <reified T> createReactiveRedisTemplate(factory: ReactiveRedisConnectionFactory) =
        ReactiveRedisTemplate(
            factory,
            RedisSerializationContext.newSerializationContext<String, T>(
                StringRedisSerializer()
            ).value(
                Jackson2JsonRedisSerializer(T::class.java).apply {
                    setObjectMapper(jacksonObjectMapper())
                }
            ).build()
        )
}