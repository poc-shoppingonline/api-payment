package com.shopping.online.apipayment.configs

import com.shopping.online.apipayment.data.repositories.MessageStreamRepo
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.context.annotation.Configuration

@EnableBinding(MessageStreamRepo::class)
@Configuration
internal class MessageStreamConfig