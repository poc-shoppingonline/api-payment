package com.shopping.online.apipayment.configs

import com.shopping.online.apipayment.utils.Constants
import io.r2dbc.spi.ConnectionFactory
import org.h2.tools.Server
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ClassPathResource
import org.springframework.data.r2dbc.connectionfactory.init.CompositeDatabasePopulator
import org.springframework.data.r2dbc.connectionfactory.init.ConnectionFactoryInitializer
import org.springframework.data.r2dbc.connectionfactory.init.ResourceDatabasePopulator
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement
import java.sql.SQLException

@Profile("dev")
@Configuration
@EnableR2dbcRepositories
@EnableTransactionManagement
class H2Configuration {

    @Value("\${spring.r2dbc.port}")
    private lateinit var port: String

    /**
     *  url: jdbc:h2:tcp://localhost:${port}/mem:${db in memory name}
     *  example: jdbc:h2:tcp://localhost:9000/mem:test
     * */
    @Bean(initMethod = "start", destroyMethod = "stop")
    @Throws(SQLException::class)
    fun h2Server(): Server = Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", port)

    @Bean
    fun initializer(connectionFactory: ConnectionFactory) =
        ConnectionFactoryInitializer().apply {
            setConnectionFactory(connectionFactory)
            setDatabasePopulator(
                CompositeDatabasePopulator().apply {
                    addPopulators(buildResourceDatabase(Constants.SCHEMA_SQL_PATH))
                    addPopulators(buildResourceDatabase(Constants.DATA_SQL_PATH))
                }
            )
        }

    private fun buildResourceDatabase(pathResource: String) =
        ResourceDatabasePopulator(
            ClassPathResource(
                pathResource
            )
        )
}