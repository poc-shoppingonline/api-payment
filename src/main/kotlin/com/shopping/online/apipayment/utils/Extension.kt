package com.shopping.online.apipayment.utils

import com.shopping.online.apipayment.dto.Response
import org.springframework.http.MediaType
import org.springframework.messaging.MessageHeaders
import org.springframework.messaging.support.MessageBuilder
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.json
import java.util.*

suspend inline fun <T> ResponseSuccess( response: T) =
    ServerResponse
        .ok()
        .json()
        .bodyValueAndAwait(
            Response(
                response
            )
        )

fun <T : Any> T.toMessagePayload() = this.let {
    MessageBuilder
        .createMessage(
            it,
            MessageHeaders(
                Collections.singletonMap<String, Any>(
                    MessageHeaders.CONTENT_TYPE,
                    MediaType.APPLICATION_JSON_VALUE
                )
            )
        )
}