package com.shopping.online.apipayment.utils

object Constants {
    const val SCHEMA_SQL_PATH = "db/schema.sql"
    const val DATA_SQL_PATH = "db/data.sql"
}

object KafkaDestinations {
    const val PRODUCE_PAYMENT = "PaymentProduce"
}

object ErrorCode {
    const val INVALID_REQUEST = "EM0001"
    const val NOT_FOUND_PROFILE = "EM0002"
    const val USER_QUALIFY_NOT_FOUND = "EM0003"
    const val SYSTEM_ERROR = "EM9999"
}