package com.shopping.online.apipayment.utils

object RedisKeyBuilder {
    fun buildCartKey(
        userId: String
    ) = "$userId"

    fun buildProductKey(
        productId: String
    ) = "$productId"
}