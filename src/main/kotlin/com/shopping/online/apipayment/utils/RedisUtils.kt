package com.shopping.online.apipayment.utils

import com.fasterxml.jackson.databind.node.ObjectNode
import com.shopping.online.apipayment.data.entities.Cart
import com.shopping.online.apipayment.data.entities.Product
import org.springframework.data.redis.core.ReactiveRedisTemplate
import org.springframework.data.redis.core.getAndAwait
import org.springframework.stereotype.Component

@Component
class RedisUtils(
    private val redisObjectNodeTemplate: ReactiveRedisTemplate<String, ObjectNode>,
    private val redisStringTemplate: ReactiveRedisTemplate<String, String>,
    private val redisRegisterCartTemplate: ReactiveRedisTemplate<String, Cart>,
    private val redisRegisterProductTemplate: ReactiveRedisTemplate<String, Product>
) {
    suspend fun getCart(key: String) = redisRegisterCartTemplate.opsForValue().getAndAwait(key)

    suspend fun getProduct(key: String) = redisRegisterProductTemplate.opsForValue().getAndAwait(key)

}