CREATE TABLE USER_PROFILE (
   USER_ID varchar(50) not null,
   BALANCE double(20) not null,
   primary key (USER_ID)
);

create table STOCK (
    PRODUCT_ID varchar(50) not null,
    AMOUNT integer(10) not null,
    primary key (PRODUCT_ID)
);